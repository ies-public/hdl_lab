% IES line plot styles
% --------------------
% Automatically applies different line colors, styles and markers to all
% line plots in the handle 'h'.
% Optionally, a custom style can be applied for a single line plot.
%
% Arguments:
% ----------
%   h:              handle of line plots (e.g. given through h = plot(x1,y1,x2,y2,...))
%
% Optional arguments:
%   marker_size:    scalar [0..inf]. Size of markers, 0 = no marker, 5.0 = default size
%   line_width:     scalar (0..inf]. Width of lines, 1.0 = default width
%
% Optional arguments only if handle points to a single line plot [h = plot(x,y)]:
% -------------------
%   style:          1x3 vector. Style of the line. Format: [color, line_style, marker_style]
%                   Value ranges: color = 1..6, line_style = 1..4, marker_style = 1..13 (see variables 'line_colors', 'line_styles' and 'marker_styles' below)

function line_plot_style(h, marker_size, line_width, style)

line_colors = [0, 0.35, 0.66;...    % TU color 1b
    0.73, 0.06, 0.13;...            % TU color 9c
    0, 0.44, 0.37;...               % TU color 3d
    0.82, 0.53, 0;...               % TU color 7c
    0.5, 0.67, 0.09;...             % TU color 4c
    0.45, 0.06, 0.52;];             % TU color 11b

line_styles = {'-';...  % straight line
    '--';...            % dashed line
    ':';...             % dotted line
    '-.'};              % dash-dot line

marker_styles = ['s';...    % square
    'o';...                 % circle
    '^';...                 % upward-pointing triangle
    'd';...                 % diamond
    '+';...                 % plus sign
    '*';...                 % asterisk
    '.';...                 % point
    'x';...                 % cross
    'v';...                 % downward-pointing triangle
    '>';...                 % right-pointing triangle
    '<';...                 % left-pointing triangle
    'p';...                 % pentagram
    'h'];                   % hexagram

% check for optional arguments:
% marker size:
if (nargin > 1)
    if (marker_size == 0)
        marker_size_final = 5;
    else
        marker_size_final = marker_size;
    end
else
    marker_size_final = 5;
end

% line width:
if (nargin > 2)
    line_width_final = line_width;
else
    line_width_final = 1;
end

% Custom style:
if (nargin > 3)
    
    % check if handle is a single line plot:
    if (size(h,1) == 1)
        
        % color:
        color = line_colors(style(1,1),:);
        
        % line style:
        line_style = line_styles{style(1,2)};
        
        % marker style:
        if ((nargin > 1) && (marker_size == 0))
            marker = 'none';
        else
            marker = marker_styles(style(1,3));
        end
        
        % Apply line style:
        set(h,'LineStyle',line_style,'Marker',marker,'MarkerSize',marker_size_final,'Color',color,'MarkerFaceColor',color,'LineWidth',line_width_final);
    else
        disp('line_plot_style(): ERROR: If using the optional argument "style", handle must be a SINGLE line plot!');
    end
else
    % Apply automatic line styles:
    for i = 1:size(h,1)
        % color:
        color = line_colors(mod(i-1,6)+1,:);
        
        % line style:
        line_style = line_styles{mod(i-1,4)+1,1};
        
        % marker style:
        if ((nargin > 1) && (marker_size == 0))
            marker = 'none';
        else
            marker = marker_styles(mod(i-1,13)+1,1);
        end
        
        % Apply line style:
        set(h(i,1),'LineStyle',line_style,'Marker',marker,'MarkerSize',marker_size_final,'Color',color,'MarkerFaceColor',color,'LineWidth',line_width_final);
    end
end

end