% shmooplot
% Create a shmoo plot of the given data

% Inputs:
%   data: MxN matrix logical (1 = fail, 0 = pass)
%   x_axis: 1xN vector with values for the x axis
%   y_axis: 1xM vector with values for the y axis

function shmooplot(data, x_axis, y_axis)

% Check dimensions:
if ((size(x_axis,2) ~= size(data,2)) || (size(y_axis,2) ~= size(data,1)))
    display('ERROR: dimensions of data and x/y values do not match!');
else
    % Set background color to white:
    set(gcf,'Color','white');
    
    % dispay data:
    image(data > 0);
    colormap([0,1,0; 1,0,0]);

    % draw grid:
    hold on;
    for i = 1:size(data,1)
       plot([.5,size(data,2)+.5],[i-.5,i-.5],'k-');
    end
    for i = 1:size(data,2)
       plot([i-.5,i-.5],[.5,size(data,1)+.5],'k-');
    end
    hold off;

    % adit axes:
    ax = gca;
    ax.XTick = 1:size(data,2);
    ax.YTick = 1:size(data,1);
    ax.XTickLabel = x_axis;
    ax.YTickLabel = y_axis;
    ax.TickLength = [0, 0];
end

end