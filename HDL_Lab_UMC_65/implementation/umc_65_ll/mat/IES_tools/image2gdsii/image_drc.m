function [drv_img, drv_sum] = image_drc(im_data, wm, dm, am, em)
% Design rule check on image data
%
% Inputs:
% - im_data:    b/w image data (MxN array)
% - wm:         minimum width constraint (px)
% - dm:         minimum distance constraint (px)
% - am:         minimum area constraint (px)
% - em:         minimum enclosure constraint (px)
%
% Outputs:
% - drv_sum:    total number of design rule violations

% Execute design rule checks:
disp('Running DRC...');

% Minimum width check:

[drv_w_h, drv_sum_w_h] = drc_hv(im_data, 'checkw', 'h', wm);    % horizontal
[drv_w_v, drv_sum_w_v] = drc_hv(im_data, 'checkw', 'v', wm);    % vertical
disp(['  Orthogonal min. width: H = ', num2str(drv_sum_w_h), '  V = ', num2str(drv_sum_w_v)]);

% Minimum distance check:
[drv_d_h, drv_sum_d_h] = drc_hv(im_data, 'checkd', 'h', dm);    % horizontal
[drv_d_v, drv_sum_d_v] = drc_hv(im_data, 'checkd', 'v', dm);    % vertical
disp(['  Orthogonal min. distance: H = ', num2str(drv_sum_d_h), '  V = ', num2str(drv_sum_d_v)]);

% Diagonal minimum width:
[drv_w_d, drv_sum_w_d] = drc_diag(im_data, 'checkw', wm);    % diagonal minimum width
disp(['  Diagonal min. width: ', num2str(drv_sum_w_d)]);

% Diagonal minimum distance:
[drv_d_d, drv_sum_d_d] = drc_diag(~im_data, 'checkd', dm);   % vertical minimum distance
disp(['  Diagonal min. distance: ', num2str(drv_sum_d_d)]);

% Minimum area:
drv_a = ~bwareafilt(im_data, [0 (am-1)]);
conn_a = bwconncomp(~drv_a);
drv_sum_a = conn_a.NumObjects;
disp(['  Min. area: ', num2str(drv_sum_a)]);

% Minimum enclosure:
drv_e = ~bwareafilt(~im_data, [0 (em-1)]);
conn_e = bwconncomp(~drv_e);
drv_sum_e = conn_e.NumObjects;
disp(['  Min. enclosure: ', num2str(drv_sum_e)]);

% combine violating pixels:
drvs = drv_w_h & drv_w_v & drv_d_h & drv_d_v & drv_w_d & drv_d_d & drv_a & drv_e;
% sum up DRVs:
drv_sum = drv_sum_w_h + drv_sum_w_v + drv_sum_d_h + drv_sum_d_v + drv_sum_w_d + drv_sum_d_d + drv_sum_a + drv_sum_e;
% build violation image:
drv_img = cat(3, (~drvs) | im_data, drvs & im_data, drvs & im_data);

% DRV summary:
disp('----------------------');
disp(['  Total DRVs: ', num2str(drv_sum)]);
disp(' ');

end