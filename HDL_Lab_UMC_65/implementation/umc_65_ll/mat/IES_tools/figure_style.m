% Figure style
% Applies a common figure style to a figure. Sets the background color to
% white.
% Optionally, the figure size can be set.

% Optional Arguments:
% figure_size:  1x4 array. Determines figure size and position. Format:
%               [xpos ypos xsize ysize] in pixels.

function figure_style(figure_size)

if (nargin > 0)
    fig_size = figure_size;
else
    fig_size = [50 50 800 400];
end

% Set figure size and background color:
set(gcf,'Position',fig_size,'Color','white');

% Set font size and grid:
set(gca,'FontSize',10,...
    'GridLineStyle','-',...
    'LineWidth',1,...
    'Box','on',...
    'XGrid','on',...
    'YGrid','on');

end