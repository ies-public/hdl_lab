#!/bin/tcsh -f
source $home/.cshrc

echo "CADENCE VIRTUOSO LAUNCHER"
echo ""

cd vir
module load 2018/cadence/umc_65_ll_eda
setenv CDS_AUTO_64BIT ALL
setenv AMSHOME /eda/cadence/2017-18/RHELx86/IC_6.1.7.715
setenv CDS_Netlisting_Mode Analog
virtuoso &
