function [im_out, drv_sum] = drc_diag(im_data, mode, w_min)
% Diagonal design rule checker/corrector
%
% Inputs:
% - im_data:    b/w image data (MxN array)
% - mode:       DRC mode ('check': marks design rule violations, 'repair': removes violating pixels)
% - w_min:      minimum width constraint (px)
%
% Outputs:
% - im_out:     b/w image data (MxN array)
% - drv_sum:    sum of design rule violations

% Generate mask:
mask = ones(w_min, w_min);
for i = 1:w_min
    for j = 1:w_min
        if (sqrt((i-1)^2 + (j-1)^2) > w_min)
            mask(i,j) = 0;
        end
    end
end

% Initialize variables:
drv_sum = 0;    % DRV counter
j = 1;          % column coordinate

% prepare output data depending on mode:
switch mode
    case 'checkw'
        im_out = ones(size(im_data,1), size(im_data,2));    % check mode highlights DRVs
    case 'checkd'
        im_out = ones(size(im_data,1), size(im_data,2));    % check mode highlights DRVs
        im_data = ~im_data;
    case 'remove'
        im_out = im_data;   % repair mode repairs violating pixels
    case 'fill'
        im_out = ~im_data;   % repair mode repairs violating pixels
        im_data = ~im_data;
end

for i = 2:size(im_data,1)-1
    while (j < (size(im_data,2)-(w_min-1)))
        % on first nonzero pixel found:
        if (im_data(i,j) > 0)
            if (j > w_min)
                % check upper neighbor for zero:
                if ((im_data(i-1,j) == 0) && (i > w_min))
                    % corner found -> check if area at corner is clean:
                    if (sum(sum(im_data(i-w_min:i-1, j-w_min:j-1) & fliplr(flipud(mask)))) > 0)
                    %if ((sum(sum(im_data(i-w_min+1:i-1, j-w_min:j-1))) + sum(im_data(i-w_min,j-w_min+1:j-1))) > 0)
                        drv_sum = drv_sum + 1;
                        % cleanup corner area:
                        im_out(i,j) = 0;
                    end
                end
                % check lower neighbor for zero:
                if ((im_data(i+1,j) == 0) && (i < (size(im_data,1)-w_min)))
                    % corner found -> check if area at corner is clean:
                    if (sum(sum(im_data(i+1:i+w_min, j-w_min:j-1) & fliplr(mask))) > 0)
                    %if ((sum(sum(im_data(i+1:i+w_min-1, j-w_min:j-1))) + sum(im_data(i+w_min,j-w_min+1:j-1))) > 0)
                        drv_sum = drv_sum + 1;
                        % cleanup corner area:
                        im_out(i,j) = 0;
                    end
                end
            end
            % move on to next edge (first zero pixel found):
            while ((im_data(i,j) > 0) && (j < (size(im_data,2)-(w_min-1))))
                j = j + 1;
            end
            j = j - 1;
            % check upper neighbor for zero:
            if ((im_data(i-1,j) == 0) && (i > w_min))
                % corner found -> check if area at corner is clean:
                if (sum(sum(im_data(i-w_min:i-1, j+1:j+w_min) & flipud(mask))) > 0)
                %if ((sum(sum(im_data(i-w_min+1:i-1, j+1:j+w_min))) + sum(im_data(i-w_min,j+1:j+w_min-1))) > 0)
                    drv_sum = drv_sum + 1;
                    % cleanup corner area:
                    im_out(i,j) = 0;
                end
            end
            % check lower neighbor for zero:
            if ((im_data(i+1,j) == 0) && (i < (size(im_data,1)-w_min)))
                % corner found -> check if area at corner is clean:
                if (sum(sum(im_data(i+1:i+w_min, j+1:j+w_min) & mask)) > 0)
                %if ((sum(sum(im_data(i+1:i+w_min-1, j+1:j+w_min))) + sum(im_data(i+w_min,j+1:j+w_min-1))) > 0)
                    drv_sum = drv_sum + 1;
                    % cleanup corner area:
                    im_out(i,j) = 0;
                end
            end
            j = j + 1;
        else
            j = j + 1;
        end
    end
    j = 1;
end

% invert after fill:
if (strcmp(mode, 'fill'))
    im_out = ~im_out;
end

end