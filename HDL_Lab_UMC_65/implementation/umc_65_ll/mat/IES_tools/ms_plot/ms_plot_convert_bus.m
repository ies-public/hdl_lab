function data_conv = ms_plot_convert_bus(data, format)
% MS_plot - A mixed signal plot tool
%
% This tool offers a way to plot several signals of different type (analog,
% digital, bus...) with a common time axis similar to the waveform view in
% Modelsim or Virtuoso.
%
% ms_plot_convert_bus() is a function to convert a digital bus signal from
% binary (character array) format to a number format.
% Inputs:
% -------
% - data:   NxM array of characters
% - format: Desired output format. Options are:
%           - 'decimal': signed integer format
%           - 'unsigned': unsigned integer format
%
% Outputs:
% --------
% - data_conv:  Converted data

% create quantizer for signed conversion:
q = quantizer([size(data,2) 0]);

% Loop over all elements:
for i = 1:size(data,1)
    % Check for X or Z:
    if ((isempty(strfind(data(i,:), 'x'))) && (isempty(strfind(data(i,:), 'z'))))
        switch format
            case 'decimal'
                data_conv(i,1) = bin2num(q, data(i,:));
            case 'unsigned'
                data_conv(i,1) = bin2dec(data(i,:));
            otherwise
                disp('ERROR: Unknown format specified!');
        end
    else
        data_conv(i,1) = NaN;   % X or Z becomes NaN
    end
end

end