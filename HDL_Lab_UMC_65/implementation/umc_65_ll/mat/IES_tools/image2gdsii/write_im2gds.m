function write_im2gds(im_data, name, px_size, image_layer, block_layer, block_purpose, mark_layer, mark_purpose)
% Write image data to GDSII file
%
% Inputs:
% - im_data:    b/w image data (MxN array)
% - name:       name of the GDS structure
% - px_size:    pixel width/height (um)
% - image_layer:    GDSII layer for the image
% - block_layer:    GDSII layer for dummy block (optional)
% - mark_layer:     GDSII layer for logo mark (optional)


% Database definitions:
user_unit = 1e-6;   % defines user unit as 1 um
db_unit = 1e-9;     % defines database unit as 1 nm

% initialize GDS structure:
gs = gds_structure(name);

% vertical mirror the image:
im_data = flipud(im_data);

% Create cell array with boundary coordinates of the individual pixels:
k = 1;
for i = 1:size(im_data,1)
    for j = 1:size(im_data,2)
        % for all non-black pixels:
        if (im_data(i,j,1) > 0)
            % Boundary coordinates of a square with height/width = "px_size" at pixel position:
            pixel{1,k} = [j*px_size, i*px_size;...
                j*px_size, i*px_size + px_size;...
                j*px_size + px_size, i*px_size + px_size;...
                j*px_size + px_size, i*px_size;...
                j*px_size, i*px_size;];
            k = k + 1;
        end
    end
end

% Add the pixel boundaries to the GDS structure:
gs(end+1) = gds_element('boundary','xy', pixel, 'layer', image_layer);

% (Optional) Add boundary coordinates of dummy block layer:
if (nargin > 4)
    block = [0, 0;...
        0, size(im_data,1)*px_size;...
        size(im_data,2)*px_size, size(im_data,1)*px_size;...
        size(im_data,2)*px_size, 0;...
        0, 0;];
    gs(end+1) = gds_element('boundary','xy', block, 'dtype', block_purpose, 'layer', block_layer);
end

% (Optional) Add boundary coordinates of logo mark layer:
if (nargin > 6)
    mark = [0, 0;...
        0, size(im_data,1)*px_size;...
        size(im_data,2)*px_size, size(im_data,1)*px_size;...
        size(im_data,2)*px_size, 0;...
        0, 0;];
    gs(end+1) = gds_element('boundary','xy', mark, 'dtype', mark_purpose, 'layer', mark_layer);
end

% Create a GDS library:
glib = gds_library(name, 'uunit', user_unit, 'dbunit', db_unit, gs);

% Write GDS file:
write_gds_library(glib, [name,'.gds']);

end