function time_string = remaining_time_string(elapsed_time, multiplier)
% Generate a string with remaining time
% Input:
% - elapsed_time: Time elapsed since loop start
% - multiplier:   Multiplier for calculation of remaining time (e.g. remaining number of steps divided by elapsed number of steps)

% claculate remaining time:
remaining_time = multiplier*elapsed_time;

% convert to hours, minutes and seconds:
remaining_hours = floor(remaining_time/3600);
remaining_minutes = floor((remaining_time-remaining_hours*3600)/60);
remaining_seconds = round(remaining_time - remaining_hours*3600 - remaining_minutes*60);

% generate string:
time_string = [num2str(remaining_hours),' h ',num2str(remaining_minutes),' m ',num2str(remaining_seconds),' s'];

end