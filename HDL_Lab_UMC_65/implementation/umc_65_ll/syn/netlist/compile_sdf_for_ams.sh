#!/bin/tcsh -f
source $home/.cshrc

####################################################
# IES SDF FILE COMPILER FOR CADENCE AMS SIMULATION #
####################################################
#
# This script searches for *.sdf files in the current
# directory, compiles them using ncsdfc and creates
# a command file template for usage with AMS Designer.
#
# TO DO AFTER COMPILATION:
# ------------------------
# In the command file template *.sdf.cmd you have to
# change the SCOPE according to your testbench hierarchy
# in Cadence Virtuoso.
# If you have multiple *.sdf files in your design, copy
# the content of all command file templates to a single
# command file.
# For more information and a tutorial for SDF back
# annotation in AMS see the IES_Project_Guide.
####################################################

# load Cadence with default setup:
module load 2023/cadence/Cadence2023

# loop over all *.sdf files found in the current directory:
foreach sdf_file (*.sdf)
	echo "Compiling $sdf_file ..."

	# compile the *.sdf file (output is a *.sdf.X file):
	ncsdfc $sdf_file

	# create a command file template if it doesn't exist yet:
	if (! -e "$sdf_file.cmd") then

		echo "Creating command file template $sdf_file.cmd ..."

		# Output command file template:
		echo COMPILED_SDF_FILE=\"${cwd}/${sdf_file}.X\", > $sdf_file.cmd
		echo SCOPE=top_level_cellview.instance, >> $sdf_file.cmd
		echo MTM_CONTROL=\"TYPICAL\", >> $sdf_file.cmd
		echo 'LOG_FILE="SDF.log";' >> $sdf_file.cmd

	else

		echo "INFO: Command file $sdf_file.cmd already exists."

	endif

	echo "done."
end
