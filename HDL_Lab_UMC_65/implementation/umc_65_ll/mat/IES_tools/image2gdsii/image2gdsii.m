%% Settings

clear all;
clc;

% USER SETTINGS:
% --------------------
% Image path and filename:
filename = 'data/ADAPT_logo.png';

% GDSII structure name:
name = 'ADAPT_logo';

% Threshold for b/w conversion:
th = 200;   % range: 1..254, 0 = automatic

% Desired pixel size:
w_pix = 0.5;    % pixel width/height (um), should be an integer fraction of "w_min" and an integer multiple of "grid"

% DRC SETTINGS:
% --------------------
% Design rules for the selected metal layer:
w_min = 2;  % minimum width (um)
d_min = 2;  % minimum distance (um)
a_min = 9;  % minimum area (um^2)
e_min = 9;  % minimum enclosure (um^2)
grid = 5;   % manufacturing grid (nm)

% LAYER SETTINGS:
% --------------------
% Image layer:
image_layer = 60;   % GDSII layer for the image (typically top-metal)

% Dummy block layer:
add_blockage = 1;   % 0 = no blockage, 1 = add dummy blockage
block_layer = 79;   % GDSII layer for dummy blockage (Optional)
block_purpose = 1;  % GDSII layer purpose for dummy blockage (Optional)

% Logo mark layer:
add_logo_mark = 1;  % 0 = no mark, 1 = add logo mark
mark_layer = 91;    % GDSII layer for logo mark (Optional)
mark_purpose = 1;   % GDSII layer purpose for logo mark (Optional)

%% Apply threshold and check image data

disp('-----------------------------------------------------------------');
disp('                   IES Image to GDSII converter');
disp('-----------------------------------------------------------------');
disp(' ');

% Derived design rules in pixels:
wm = w_min/w_pix;       % minimum width (px)
dm = d_min/w_pix;       % minimum distance (px)
am = a_min/(w_pix^2);   % minimum area (px)
em = e_min/(w_pix^2);   % minimum enclosure (px)

if (th == 0)    % automatic threshold
    
    % search for ideal threshold (minimum number of DRVs):
    disp('Running automatic threshold search...');
    th_iter = 5:5:250;
    h = init_waitbar();
    for k = 1:size(th_iter,2)
        % Read image data:
        im_data = (mean(imread(filename),3) <= th_iter(1,k));
        % Run DRC:
        [drv_img(:,:,:,k), drv_sum(k)] = image_drc(im_data, wm, dm, am, em);
        k = k + 1;
        update_waitbar(h, k, size(th_iter,2));
    end
    close_waitbar(h);
    [drv_min, ind] = min(drv_sum);  % find minimum
    disp(['Generating B/W image using threshold: ',num2str(th_iter(ind)), ' with ', num2str(drv_min), ' initial DRVs']);
    disp(' ');
    
    % plot search results:
    figure('Name','Automatic threshold result');
    h = plot(th_iter, drv_sum, th_iter(ind), drv_min);
    line_plot_style(h(1), 0);
    line_plot_style(h(2), 10, 1, [2 1 6]);
    figure_style();
    xlabel('threshold');
    ylabel('number of DRVs');
    legend('search results','ideal threshold', 'Location', 'NorthEast');
    
    % show DRVs:
    figure('Name','DRV Viewer');
    imshow(double(drv_img(:,:,:,ind)));
    
    % generate final image data:
    im_data = (mean(imread(filename),3) <= th_iter(1,ind));
    drv_sum = drv_sum(1,ind);
    
elseif (th > 254)
    
    disp('ERROR: Threshold must be within range 1..254 or 0!');
    
else
    
    % Read image data:
    im_data = (mean(imread(filename),3) <= th);
    % Run DRC:
    [drv_img, drv_sum] = image_drc(im_data, wm, dm, am, em);
    figure('Name','DRV Viewer');
    imshow(double(drv_img));
    disp(['Generating B/W image with user threshold: ', num2str(th)]);
    disp(' ');
    
end

%% Correct DRVs

disp('Running DRV correction...');

% Initial step: Fix min. area and min. enclosure violations:
% ------------------------------------

% Remove areas smaller than min. area:
disp('  Remove min. area violations')
im_data = bwareaopen(im_data, am);

% Fill holes smaller than min. enclosure:
disp('  Fill min. enclosure violations');
im_data = ~bwareaopen(~im_data, em);

% Fix min. width and min. distance violations:
% ------------------------------------

N_iter = 10;    % Maximum number of iterations for each loop

% Initialize variables:
drv_sum_all(1,1) = drv_sum;
drv_sum_all(1,2) = drv_sum;
drv_sum_old = drv_sum;
done = 0;
n = 1;
while ((done == 0) && (n <= N_iter))
    
    % Run orthogonal DRV fixing until no more fixes possible:
    disp('  Orthogonal DRV fixing');
    hv_done = 0;
    m = 1;
    while ((hv_done == 0) && (m <= N_iter))
        % horizontal min. width:
        [im_data, drv_sum_w(1,1)] = drc_hv(im_data, 'remove', 'h', wm);
        % vertical min. width:
        [im_data, drv_sum_w(1,2)] = drc_hv(im_data, 'remove', 'v', wm);
        disp(['    Min. width: H = ', num2str(drv_sum_w(1,1)), '  V = ', num2str(drv_sum_w(1,2))]);
        % horizontal min. distance:
        [im_data, drv_sum_w(1,3)] = drc_hv(im_data, 'fill', 'h', dm);
        % vertical min. distance:
        [im_data, drv_sum_w(1,4)] = drc_hv(im_data, 'fill', 'v', dm);
        disp(['    Min. distance: H = ', num2str(drv_sum_w(1,3)), '  V = ', num2str(drv_sum_w(1,4))]);
        drv_sum_all(1,1) = sum(drv_sum_w);
        % Check for infinite loop:
        if (drv_sum_all(1,1) == 0)
            hv_done = 1;
            disp('    All orthogonal DRVs fixed');
        else
            if ((m > 1) && isequal(drv_sum_w, drv_sum_w_old))
                hv_done = 1;
                disp('    No more fixing possible');
            end
        end
        drv_sum_w_old = drv_sum_w;
        m = m + 1;
    end
    
    % Run diagonal DRV fixing until no more fixes possible:
    disp('  Diagonal DRV fixing');
    diag_done = 0;
    m = 1;
    while ((diag_done == 0) && (m <= N_iter))
        % digonal min. width:
        [im_data, drv_sum_d(1,1)] = drc_diag(im_data, 'remove', wm);
        disp(['    Min. width: ', num2str(drv_sum_d(1,1))]);
        % diagonal min. distance:
        [im_data, drv_sum_d(1,2)] = drc_diag(im_data, 'fill', dm);
        disp(['    Min. distance: ', num2str(drv_sum_d(1,2))]);
        drv_sum_all(1,2) = sum(drv_sum_d);
        % Check for infinite loop:
        if (drv_sum_all(1,2) == 0)
            diag_done = 1;
            disp('    All diagonal DRVs fixed');
        else
            if ((m > 1) && isequal(drv_sum_d, drv_sum_d_old))
                diag_done = 1;
                disp('    No more fixing possible');
            end
        end
        drv_sum_d_old = drv_sum_d;
        m = m + 1;
    end
    
    drv_sum_old = drv_sum;
    drv_sum = sum(drv_sum_all);
    disp(['  Total remaining DRVs: ', num2str(drv_sum)]);

    % Check for infinite loop:
    if ((n > 1) && isequal(drv_sum_all, drv_sum_all_old))
        done = 1;
        if (drv_sum == 0)
            disp('  All DRVs fixed');
        else
            disp('  No more fixing possible');
        end
    end
    drv_sum_all_old = drv_sum_all;
    
    n = n + 1;
end

% Final step: Fix min. area and min. enclosure violations:
% ------------------------------------

% Remove areas smaller than min. area:
disp('  Remove min. area violations')
im_data = bwareaopen(im_data, am);

% Fill holes smaller than min. enclosure:
disp('  Fill min. enclosure violations');
im_data = ~bwareaopen(~im_data, em);

% DRV correction summary:
if (sum(drv_sum_all) == 0)
    disp('----------------------');
    disp('  All DRVs fixed');
else
    disp('----------------------');
    disp(['  DRV correction finished with ', num2str(sum(drv_sum_all)), ' remaining DRVs']);
end
disp(' ');

% Show result:
figure('Name','Final image');
imshow(im_data);

%% Write GDSII file

% Write GDSII file:
disp(['Writing GDSII output file: ', name, '.gds']);
if (add_blockage == 1)
    if (add_logo_mark == 1)
        write_im2gds(im_data, name, w_pix, image_layer, block_layer, block_purpose, mark_layer, mark_purpose);
    else
        write_im2gds(im_data, name, w_pix, image_layer, block_layer, block_purpose);
    end
else
    write_im2gds(im_data, name, w_pix, image_layer);
end