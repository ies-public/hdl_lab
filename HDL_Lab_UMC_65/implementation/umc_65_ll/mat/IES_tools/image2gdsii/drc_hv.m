function [im_out, drv_sum] = drc_hv(im_data, mode, direction, w_min)
% Horizontal/Vertical design rule checker/corrector
%
% Inputs:
% - im_data:    b/w image data (MxN array)
% - mode:       DRC mode ('checkw' = find min. width DRVs, 'checkd' = find min. distance DRVs, 'remove' = remove min. width violating pixels, 'fill' = fill min. distance violating gaps)
% - direction:  'h' = horizontal, 'v' = vertical
% - w_min:      minimum width constraint (px)
%
% Outputs:
% - im_out:     b/w image data (MxN array)
% - drv_sum:    sum of design rule violations


% Initialize variables:
w_sum = 0;      % width counter
drv_sum = 0;    % DRV counter
j = 1;          % column coordinate

% Transpose for vertical check:
if (direction == 'v')
    im_data = im_data';
end

% prepare output data depending on mode:
switch mode
    case 'checkw'
        im_out = ones(size(im_data,1), size(im_data,2));    % find minimum width violations
    case 'checkd'
        im_out = ones(size(im_data,1), size(im_data,2));    % find minimum distance violations
        im_data = ~im_data;
    case 'remove'
        im_out = im_data;   % remove minimum width violating pixels
    case 'fill'
        im_out = ~im_data;  % fill minimum distance violating gaps
        im_data = ~im_data;
end

% All lines:
for i = 1:size(im_data,1)
    % Columns:
    while (j < (size(im_data,2)-(w_min-1)))
        % Find next edge:
        if (im_data(i,j) > 0)
            j_start = j;
            % calculate width:
            while ((im_data(i,j) > 0) && (j < (size(im_data,2)-(w_min-1))))
                w_sum = w_sum + 1;
                j = j + 1;
            end
            % check minimum width rule:
            if (w_sum < w_min)
                % increment DRV counter:
                drv_sum = drv_sum + 1;
                % mark/repair violating pixels:
                im_out(i,j_start:j-1) = zeros(1,j-j_start);
            end
            % reset width counter:
            w_sum = 0;
        else
            j = j + 1;
        end
    end
    j = 1;
end

% invert after fill:
if (strcmp(mode, 'fill'))
    im_out = ~im_out;
end

% Transpose after vertical check:
if (direction == 'v')
    im_out = im_out';
end

end