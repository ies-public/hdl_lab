#################################
# SETUP THE DESIGN
#################################

# Check that the design is free of latches:
set hdlin_check_no_latch true

# Add the line below if your design is using a synchronous reset:
# set hdlin_ff_always_sync_set_reset true

### DEFINE DESIGN FILES #########################################
#
# Set the name of the top-module:
set TOP_LEVEL_MODULE	"YOUR_TOPMODULE_NAME"
#
# List all source files of the design:
set FILE_LIST	{Your_Verilog_Files.v}
#
#################################################################

#set verilogout_no_tri true

# Create subdirectories for temporary files to keep syn folder clean:
if [file exists results] {
  file delete -force results
}
file mkdir results
set_vsdc results/design.vsdc
set_svf  results/design.svf

if [file exists WORK] {
  file delete -force WORK
}
file mkdir WORK
define_design_lib WORK -path ./WORK
