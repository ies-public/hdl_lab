function h = ms_plot(n, time_axis, time_lim, custom_width)
% MS_plot - A mixed signal plot tool
%
% This tool offers a way to plot several signals of different type (analog,
% digital, bus...) with a common time axis similar to the waveform view in
% Modelsim or Virtuoso.
%
% ms_plot() is the basic function to create a new mixed signal plot figure.
% Inputs:
% -------
% - n:              (Optional) create a figure with number n
% - time_axis:      (Optional) custom label for the time axis (Default is: "Time (s)")
% - custom_width:   (Optional) custom figure width (0 = fit to screen, default: 1600 px)
%
% Outputs:
% --------
% - h:  Handle to the ms_plot figure
%
% Usage example:
% --------------
% h = ms_plot();
% h = ms_plot_add(h, time, data, ...)
% h = ms_plot_add(h, ...)
% ...

% If requested, create figure with number:
if (nargin > 0)
    h.fig = figure(n);
else
    h.fig = figure();
end

% If requested make time axis fixed to given limits:
if (nargin > 2)
    if (time_lim(2) > time_lim(1))
        h.time_lim = time_lim;      % Valid limits
    else
        h.time_lim = [-Inf Inf];    % Invalid limits -> no limits
    end
else
    h.time_lim = [-Inf Inf];        % default: no limits
end

% Custom window width if requested:
if (nargin > 3)
    if (custom_width <= 0)
        % fit to screen:
        screensize = get( groot, 'Screensize' );
        width = screensize(3);
    else
        % use custom width:
        width = custom_width;
    end
else
    % use default width:
    width = 1600;
end

% Initialize handle:
h.units = [];
h.ax = [];
h.type = [];

% Set figure size and position:
h.unit_height = 40;     % unit plot height = 50 px
h.top_margin = 50;      % time axis height in px
h.bottom_margin = 5;    % bottom_margin creates some extra space at the bottom to have a fully visible outline
height = (h.top_margin+h.bottom_margin);
set(gcf,'Position',[50 50 width height],'Color','white');

% Additional margin for digital plots ('digital', 'bus' and 'splitbus'):
h.tb_margin = 7;    % top and bottom margin in px

% Create background axes for time scale:
h.timeax = axes();

% Configure time axis and grid appearance:
set(h.timeax, 'Box', 'off');                % Only top axis
set(h.timeax, 'XAxisLocation', 'top');      % Axis location top
if (nargin > 1)
    xlabel(h.timeax, time_axis);            % Custom time axis label
else
    xlabel(h.timeax, 'Time (s)');           % Default label
end
set(h.timeax, 'YTick', 1);                  % Y axis ticks
set(h.timeax, 'YTickLabel', '')             % remove tick labels on Y axis
set(h.timeax, 'TickLength', [0.002 0.002]); % shorter ticks
set(h.timeax, 'XGrid', 'on');               % enable time grid
set(h.timeax, 'GridLineStyle', ':');        % set grid style
set(h.timeax, 'GridAlpha', 0.2);            % set grid visibility
set(h.timeax, 'YColor', [1 1 1]);           % remove Y axis (set to white)
set(h.timeax, 'TickDir', 'out');            % outside ticks

% Position the axes:
tiax = get(h.timeax, 'TightInset');         % get best fit for time axis
set(h.timeax, 'Position', [tiax(1)...
                           (tiax(2) + h.bottom_margin/height)...
                           (1 - tiax(1) - tiax(3))...
                           (1 - tiax(2) - h.bottom_margin/height - tiax(4))]); % Set time axis position

end