#!/bin/tcsh -f
source $home/.cshrc

echo "CADENCE ENCOUNTER LAUNCHER"
echo ""

cd enc
module load 2022/cadence/umc_65_ll
setenv CDS_AUTO_64BIT ALL
innovus
