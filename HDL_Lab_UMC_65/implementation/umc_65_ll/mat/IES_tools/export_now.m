function export_now(filename)
% export current active figure as .png and .pdf with white background.
% Export location is subfolder "plots"

% set white background:
set(gcf,'Color','white');

% Export png:
export_fig(['plots/',filename,'.png'],'-png','-m2');

% Export pdf:
export_fig(['plots/',filename,'.pdf'],'-pdf');

% Info to ignore error messages:
disp('INFO: Ignore any error messages above if files have been created!');

end