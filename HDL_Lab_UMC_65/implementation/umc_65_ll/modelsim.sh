#!/bin/tcsh -f
source $home/.cshrc

echo "MODELSIM LAUNCHER"
echo ""

cd src
module load modelsim/10.7c
# -novopt                 Force incremental mode (pre-6.0 behavior)
vsim -novopt
