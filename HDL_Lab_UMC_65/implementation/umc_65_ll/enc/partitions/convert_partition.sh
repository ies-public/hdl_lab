#!/bin/tcsh -f
source $home/.cshrc

######################################
# PARTITION CONVERTER
######################################

echo "PARTITION CONVERTER"
echo "for cadence encounter"
echo ""
echo "Enter partition name:"
set partition = ($<)

if (-e $partition) then
	# rename partition folder:
	echo "renaming partition folder..."
	mv $partition $partition.enc.dat

	# create project file:
	echo "creating project file..."
	echo "restoreDesign $partition.enc.dat $partition" >! $partition.enc
	
	echo "DONE."
else
	echo "ERROR: partition not found!"
endif
