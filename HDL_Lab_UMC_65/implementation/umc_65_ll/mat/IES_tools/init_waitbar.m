function h_wb = init_waitbar()
% Open a waitbar with remaining time display
% Output:
% - h_wb: Handle

% Initialize waitbar:
h = waitbar(0,'Initializing...');

% Set start point for timer:
t_start = tic;

% Return handles of waitbar and Timer:
h_wb = {h, t_start};

end