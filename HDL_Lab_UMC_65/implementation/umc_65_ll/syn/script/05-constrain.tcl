#################################
# CONSTRAIN THE DESIGN
#################################
# IMPORTANT: Nearly all values in this file need to be adapted to your design!
#            You might not need all constraints listed, these are just examples!
#
# The units of the constraints depend on the library used. Typical units are: time = ns, resistance = kOhm, capacitance = pF
# Check the units of your library in the following way:
# - Open DesignVision
# - Source the technology setup script (source 01-setup_tech.tcl)
# - Type: list_lib
# - Find out the name of your library (it may differ from the filename)
# - Type: report_lib library_name

#################################
# Constrain asynchronous reset:
#################################
# ToDo:
# - Set the network coming from your reset port as ideal network
# - Set the false Path for a falling edge of the reset signal
# - Set the model latency to a maximum of 0 ns
# - Set the model transition time to a maximum of 0.03 ns

# Set the net connected to the reset pin ideal net for synthesis:
set_ideal_network ...

# Async reset assertion timing is not important (only the falling edge / de-assertion is important):
set_false_path ...

# Set the maximum latency of the reset signal to 0 ns:
set_ideal_latency ...

# Set the maximum transition time of the reset pin to 30ps:
set_ideal_transition ...

#Idea how your could constrain a generated Clock

#create_generated_clock -name SCLOCK -source [get_port clk] -divide_by 2 [get_port ctrl/Clk_Filter]
#set_clock_uncertainty ... [get_clocks SCLOCK]
#...


#################################
# Constrain clock:
#################################
# ToDo:
# - Create a Clock signal with the desired Frequency with the Name "CLOCK" (you might want to simply start with 1kHz but add possible frequencys later.)
# - Change the clock period to match your design if you want to synthesize for higher frequencys. 
# - Set the clock latency to a maximum of 0 ns and the clock transition time to a maximum of 0.1 ns.
# - Create a Jitter of +- 30ps (Set the setup and the hold uncertainty.)

# Default unit for all timing constraints: ns

# create clock constraint:
create_clock ...

# model clock network insertion delay (from clk root to synchronous elements):
set_clock_latency ...

# model clock transisition time:
set_clock_transition ...

# model clock uncertainty (jitter + skew):
set_clock_uncertainty ...
set_clock_uncertainty ...


#################################
# (Optional) Constrain inputs:
#################################
# model input delay:
#set_input_delay -max 2.5  -clock CLOCK [remove_from_collection [all_inputs] clk_sample_in] -network_latency_included
#set_input_delay -min 0.01 -clock CLOCK [remove_from_collection [all_inputs] clk_sample_in] -network_latency_included

# model drive resistance of inputs (unit depends on library, typ. kOhm):
#set_drive -max 1    [remove_from_collection [all_inputs] clk]
#set_drive -min 0.01 [remove_from_collection [all_inputs] clk]


#################################
# (Optional) Constrain outputs:
#################################
# model output delay:
#set_output_delay -max 2.5  -clock sar_clock [all_outputs] -network_latency_included
#set_output_delay -min 0.01 -clock sar_clock [all_outputs] -network_latency_included

# model load capacitance (unit depends on library, typ. pF):
#set_load -max -pin_load 0.8  [all_outputs]
#set_load -max -pin_load 0.5  [get_ports *busy*]
#set_load -max -pin_load 0.1  [get_ports *sar_out*]
#set_load -min -pin_load 0.001 [all_outputs]


################################# 
# (Optional) Electrical Design Rule Constraints
# check specification in library
#################################
set_max_transition 1 [all_outputs]
set_max_transition 1 [all_inputs]


#################################
# (Optional) 5% WC OCV Timing Derate
# For local cell and interconnect delay variation
#################################
#set_timing_derate -early 0.95
# Derate applies to cell and incterconnect delays

