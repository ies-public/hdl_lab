% dnl_inl
% This function calculates differential (DNL) and integral (INL) non-linearity of the
% given data in terms of LSB and shows a plot of them. The output plot can be disabled.

% Inputs:
%   data: Nx2 matrix, column 1: x-values, column 2: y-values
%   plot_disable: can be anything, disables output plot
%
% Outputs:
%   dnl: N-1x1 vector of DNL values
%   inl: N-1x1 vector of INL values

function [dnl, inl] = dnl_inl(data, plot_disable)

% calculate DNL:
dnl = (data(2:end,2)-data(1:end-1,2))./((data(end,2)-data(1,2))/(size(data,1)-1))-1;

% calculate INL:
inl = (data(2:end,2)-data(1,2))./((data(end,2)-data(1,2))/(size(data,1)-1))-(1:(size(data,1)-1))';

% check if plot output is enabled:
if (nargin == 1)
    
    % generate x-values:
    steps = 1:(size(data,1)-1);
    
    % plot dnl and inl:
    h = plot(steps, dnl, steps, inl);
    % apply styles:
    figure_style();
    line_plot_style(h, 0);
    % edit axes:
    xlim([steps(1) steps(end)]);
    xlabel('Step (LSB)');
    ylabel('DNL/INL (LSB)');
    % legend and title:
    title('DNL and INL');
    legend('DNL', 'INL');
end

end