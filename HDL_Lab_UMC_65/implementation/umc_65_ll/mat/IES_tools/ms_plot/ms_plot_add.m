function h = ms_plot_add(h, time, data, type, name, unit, unit_height, legend_entries)
% MS_plot - A mixed signal plot tool
%
% This tool offers a way to plot several signals of different type (analog,
% digital, bus...) with a common time axis similar to the waveform view in
% Modelsim or Virtuoso.
%
% ms_plot_add() is the function for adding a new signal to an existing
% ms_plot figure.
% Inputs:
% -------
% - h:              Handle to existing ms_plot figure (created by h = ms_plot())
% - time:           NxM matrix of scalar time values (M > 1: multiple signals in a single plot)
% - data:           NxM matrix of scalar data values
% - type:           Signal type. Options are:
%                       - 'digital': single bit digital signal
%                       - 'analog': analog singal
%                       - 'd2a': multibit digital signal drawn as analog signal
%                       - 'bus': multibit digital signal drawn as combined bus
%                       - 'splitbus': multibit digital singal, each bit drawn seperately
% - name:           Signal name shown on the left side of the plot
% - unit:           (Optional) Label for the y axis
% - unit_height:    (Optional) custom height of the plot in unit heights (defaults see below), 0 = use default
% - legend_entries: (Optional) 1xM cell array of signal names shown in a legend inside the plot
%
% Outputs:
% --------
% - h:  Handle to the ms_plot figure

% Check time and data dimension consistency:
if (size(time) ~= size(data))
    disp('ERROR: time and data array dimensions do not match!');
end

% If requested use custom unit height:
if ((nargin > 6) && (unit_height > 0))
    height = unit_height;
else
    % Select plot height in unit heights (50 px) based on signal type:
    switch type
        case 'digital'
            height = 1;
        case 'd2a'
            height = 4;
        case 'analog'
            height = 4;
        case 'bus'
            height = 1;
        case 'splitbus'
            height = size(data,2);
        otherwise
            disp('ERROR: unknown type!');
            height = 4;
    end
end

% Add height to list:
h.units(end+1,1) = height;

% Make the existing figure the current figure:
figure(h.fig);

% Set figure size and position:
pos_old = get(gcf, 'Position');     % get previous size and position
fig_height = sum(h.units)*h.unit_height + h.bottom_margin + h.top_margin;
set(gcf,'Position',[pos_old(1) pos_old(2) pos_old(3) fig_height],'Color','white');  % rescale height

% Get minima and maxima of time vectors:
time_min = max(min(min(time)), h.time_lim(1));
time_max = min(max(max(time)), h.time_lim(2));

% Configure time axis and grid appearance:
if (size(h.ax,1) > 0)
    time_old = get(h.timeax, 'XLim');
    set(h.timeax, 'XLim', [min(time_min, time_old(1)) max(time_max, time_old(2))]); % if other plots exist, change scale only if required
else
    set(h.timeax, 'XLim', [time_min time_max]);     % if first plot: set scale according to data
end

%% Data plot

% create new axes:
axes();

% Get time limits:
time_lim = get(h.timeax, 'XLim');   % get time axis limits

% plot data depending on signal type:
switch type
    case 'analog'
        % Plot the data:
        [ax, p1, p2] = plotyy(0,0,time, data, 'plot', 'plot');  % continous data plot
        
        % Apply plot style:
        line_plot_style(p2, 0);     % apply automatic plot style(s)
        
    case 'bus'
        % Calculate transition time:
        trans_width = 3;                    % Desired transistion width (px)
        fig_pos = get(h.fig, 'Position');   % Get figure size
        ax_pos = get(h.timeax, 'Position'); % get size of time axis
        ax_width = ax_pos(3)*fig_pos(3);    % calculate axis width in pixels
        tr = trans_width/ax_width*(time_lim(2) - time_lim(1));  % calculate transition width in time units
        
        % initilize variables:
        d = data(1,1);
        value = 0;
        time_bus = time(1,1);
        if (isnan(d))
            data_bus = NaN;
        else
            data_bus = value;
        end
        label_bus = data(1,1);
        j = 2;
        
        
        
        % Loop over all data elements:
        for i = 2:size(time,1)
            % check for signal changes:
            if (data(i,1) ~= d)
                % signal transition start:
                if (isnan(data(i-1,1)))
                    % handle NaN as X:
                    time_bus(j,1) = time(i,1);
                    data_bus(j,1) = NaN;
                else
                    time_bus(j,1) = time(i,1) - tr;
                    data_bus(j,1) = value;
                end
                value = 1 - value;
                label_bus(j,1) = data(i-1,1);
                j = j + 1;
                
                % signal transition end:
                if (isnan(data(i,1)))
                    % Handle NaN as X:
                    time_bus(j,1) = time(i,1);
                    data_bus(j,1) = NaN;
                else
                    time_bus(j,1) = time(i,1) + tr;
                    data_bus(j,1) = value;
                end
                label_bus(j,1) = data(i,1);
                j = j + 1;
                
                d = data(i,1);
            end
        end
        % Replicate last data point at end of time axis:
        time_bus(j,1) = time_lim(2);
        data_bus(j,1) = data_bus(j-1,1);
        label_bus(j,1) = d;
        
        assignin('base', 'bla', data_bus);
        
        % Create signal for X values:
        datax_bus = isnan(data_bus).*0.5;
        datax_bus(find(~datax_bus)) = NaN;
        data_bus(isnan(data_bus)) = 0.5;

        % plot signal:
        [ax, p1, p2] = plotyy(0,0,[time_bus, time_bus, time_bus], [data_bus, 1-data_bus, datax_bus], 'plot', 'plot');
        
        % Apply plot style:
        line_plot_style(p2(1), 0);
        line_plot_style(p2(2), 0);
        line_plot_style(p2(3), 0, 1, [2 1 1]);  % X states in red
        
    case 'splitbus'
        % Loop over bits:
        for i = 1:size(data,2)
            % Loop over data:
            for j = 1:size(data,1)
                % Create signals:
                switch data(j,size(data,2)-i+1)
                    case '0'
                        data_bus(j,i) = 0.15 + i;
                        datax_bus(j,i) = NaN;
                    case '1'
                        data_bus(j,i) = 0.85 + i;
                        datax_bus(j,i) = NaN;
                    otherwise
                        % Handle X/Z/NaN:
                        data_bus(j,i) = 0.5 + i;
                        datax_bus(j,i) = 0.5 + i;
                end
            end
            % create labels for bits:
            labels{i} = ['[', num2str(i-1), ']'];
            label_pos(i) = 0.5 + i;
        end
        % Replicate last data point at end of time axis:
        time_bus = [time; time_lim(2)];
        data_bus = [data_bus; data_bus(end,:)];
        datax_bus = [datax_bus; datax_bus(end,:)];
        
        % Plot signals:
        [ax, p1, p2] = plotyy(0,0,repmat(time_bus,1,size(data_bus,2)*2), [data_bus, datax_bus], 'plot', 'stairs');
        
        % Apply plot style:
        for i = 1:size(data_bus,2)
            line_plot_style(p2(i,1), 0, 1, [1 1 1]);
        end
        for i = size(data_bus,2)+1:2*size(data_bus,2)
            line_plot_style(p2(i,1), 0, 1, [2 1 1]);    % X/Z states in red
        end
        
    case 'digital' 
        % Handle NaN as X states:
        nans = isnan(data);
        datax = [nans; nans(end,1)].*0.5;
        datax(find(~datax)) = NaN;
        data(isnan(data)) = 0.5;    % replace NaN with 0.5 (= X or Z)
        
        % Replicate last data point at end of time axis:
        time = [time; time_lim(2)];
        data = [data; data(end,:)];
        
        % Plot signal:
        [ax, p1, p2] = plotyy(0,0,[time, time], [data, datax], 'plot', 'stairs');
        
        % Apply plot style:
        line_plot_style(p2(1,1), 0, 1, [1 1 1]);
        line_plot_style(p2(2,1), 0, 1, [2 1 1]);
    case 'd2a'
        % Plot signal:
        [ax, p1, p2] = plotyy(0,0,time, data, 'plot', 'stairs');
        
        % Apply plot style:
        line_plot_style(p2, 0);
    otherwise
        % Plot signal:
        [ax, p1, p2] = plotyy(0,0,time, data, 'plot', 'stairs');
        
        % Apply plot style:
        line_plot_style(p2, 0);
end

% Add axes handle and type to list:
h.ax(end+1,:) = ax;
h.type{end+1,1} = type;

% Configure time axis and grid appearance:
set(h.ax(end,1), 'XLim', get(h.timeax, 'XLim'));
set(h.ax(end,2), 'XLim', get(h.timeax, 'XLim'));
set(h.ax(end,1), 'XAxisLocation', 'top');
set(h.ax(end,2), 'XAxisLocation', 'top');
set(h.ax(end,1), 'XTickLabel', {});
set(h.ax(end,2), 'XTickLabel', {});
set(h.ax(end,1), 'XColor', [0.8 0.8 0.8]);
set(h.ax(end,1), 'GridAlpha', 1);
set(h.ax(end,1), 'TickLength', [0 0]);
set(h.ax(end,1), 'XGrid', 'on');
set(h.ax(end,1), 'GridLineStyle', '-');

% Configure Signal name axis appearance:
ylim(h.ax(end,1), [0 1]);
set(h.ax(end,1), 'YTick', 0.5);
set(h.ax(end,1), 'YTickLabel', name);
% set(h.ax(end,1), 'YColor', [0 0.35 0.66]); % TU color 1b
set(h.ax(end,1), 'YColor', [0.15 0.15 0.15]);
% set(h.ax(end,2), 'YColor', [0.6 0.6 0.6]);
%set(h.ax(end,1), 'YColor', [0.35 0.52 0.76]);
set(h.ax(end,1), 'YGrid', 'off');
set(h.ax(end,1), 'FontSize', 12);

% Configure Signal range axis appearance:
set(h.ax(end,2), 'YColor', [0.15 0.15 0.15]);
set(h.ax(end,2), 'TickLength', [0.002 0.002]);
set(h.ax(end,2), 'YGrid', 'on');
set(h.ax(end,2), 'GridLineStyle', '-');
set(h.ax(end,2), 'GridAlpha', 0.2);
switch type
    case 'digital'
        set(h.ax(end,2), 'YTick', []);  % no ticks on digital signals
    case 'bus'
        set(h.ax(end,2), 'YTick', []);  % no ticks on bus signals
    case 'splitbus'
        % Bit numbers as tick labels:
        set(h.ax(end,2), 'YTick', label_pos);
        set(h.ax(end,2), 'YTickLabel', labels);
        
        % Add a Y grid:
        set(h.ax(end,2), 'GridLineStyle', ':');
        set(h.ax(end,2), 'YMinorGrid', 'on');
        set(h.ax(end,2), 'TickLength', [0 0]);
        set(h.ax(end,2), 'MinorGridLineStyle', '-');
    otherwise
        % Add a Y grid:
        set(h.ax(end,2), 'YMinorGrid', 'on');
        set(h.ax(end,2), 'MinorGridLineStyle', ':');
        set(h.ax(end,2), 'MinorGridAlpha', 0.4);
end
% Create signal axis label if requested:
if (nargin > 5)
    ylabel(h.ax(end,2), unit);
end

% Create legend if requested:
if (nargin > 6)
    legend(h.ax(end,2), legend_entries, 'Location', 'East');
end

% Add transitions and data labels to bus signals:
if (strcmp(type, 'bus'))
    d = NaN;
    % Loop over time points:
    for i = 1:size(time_bus,1)
        % Detect signal changes:
        if (label_bus(i,1) ~= d)
            % Omit last label and non-visible labels:
            if ((i < size(time_bus,1)) && (time_bus(i,1) < h.time_lim(2)) && (time_bus(i,1) > h.time_lim(1)))
                % Label only if not NaN:
                if (~isnan(label_bus(i,1)))
                    % Create label text:
                    tx = text(time_bus(i,1) + (time_bus(i+1,1) - time_bus(i,1))/2, 0.5, num2str(label_bus(i,1)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'Color', [0, 0.35, 0.66]);
                    
                    % Remove label if it is too wide:
                    te = get(tx, 'Extent');
                    if (te(3) > 1.5*((time_bus(i+1,1) - time_bus(i,1)) + 2*tr))     % factor 1.5 allows for slightly bigger labels, should be still readable
                        delete(tx);
                    end
                    % Remove label if outside plot:
                    if ((te(1) < h.time_lim(1)) || ((te(1) + te(3)) > h.time_lim(2)))
                        delete(tx);
                    end
                end
            end
            d = label_bus(i,1);
        end
    end
end

% Calculate best fit for all axes:
ti = max(get(h.ax(end,1), 'TightInset'), get(h.ax(end,2), 'TightInset'));
posax = get(h.timeax, 'Position');
ti_final = [max(ti(1), posax(1))...
            ti(2)...
            max(ti(3), (1-(posax(3)+posax(1))))...
            ti(4)];

% Re-adjust all axes:
for i = 1:size(h.ax,1)
    % Readjust time axis:
    set(h.ax(i,1), 'XLim', get(h.timeax, 'XLim'));
    set(h.ax(i,2), 'XLim', get(h.timeax, 'XLim'));
    % Position data axes:
    if (i < size(h.ax,1))
        offset = sum(h.units(i+1:end,1))*h.unit_height/fig_height;
    else
        offset = 0;
    end
    ti = max(get(h.ax(i,1), 'TightInset'), get(h.ax(i,2), 'TightInset'));
    % Add some margin on top and bottom for digital, bus and splitbus
    % signals (otherwise they are too close to each other because they have
    % no Y ticks):
    if (strcmp(h.type{i,1}, 'digital') || strcmp(h.type{i,1}, 'bus') || strcmp(h.type{i,1}, 'splitbus'))
        tb_margin = h.tb_margin/fig_height;
    else
        tb_margin = 0;
    end
    % Adjust position:
    set(h.ax(i,1), 'Position', [ti_final(1)...
                                ti(2) + h.bottom_margin/fig_height + offset + tb_margin...
                                (1-ti_final(1) - ti_final(3))...
                                (h.units(i,1)/(fig_height/h.unit_height) - ti(2) - 2*tb_margin - ti(4))]);
end

% Re-adjust position of time axes:
tiax = get(h.timeax, 'TightInset');
set(h.timeax, 'Position', [ti_final(1)...
                           tiax(2) + h.bottom_margin/fig_height...
                           (1 - ti_final(1) - ti_final(3))...
                           (1 - tiax(2) - h.bottom_margin/fig_height - tiax(4))]);

end